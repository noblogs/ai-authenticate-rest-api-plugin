<?php
/**
 * Plugin Name: ai-authenticate-rest-api
 * Plugin URI: https://git.autistici.org/noblogs/ai-authenticate-rest-api-plugin
 * Description: Ensure REST API access is authenticated
 * Version: 0.0.4
 * Author: Autistici/Inventati
 * Author URI: https://www.autistici.org/
 * License: MIT
 * License URI: http://opensource.org/licenses/MIT
 */

function ai_authenticate_rest_api_is_wp_rest_api_request() {
    // Identify "core" WP REST API requests, by their URL prefix.
    return strncmp(
        $_SERVER['REQUEST_URI'],
        '/wp/v2/',
        strlen('/wp/v2/')) == 0;
}

add_filter('rest_authentication_errors', function($result) {
    // If a previous authentication check was applied,
    // pass that result along without modification.
    if (true === $result || is_wp_error($result)) {
        return $result;
    }
 
    // No authentication has been performed yet.
    // Return an error if user is not logged in, but only if we
    // think the request is for the main WP REST API.
    if (!is_user_logged_in() &&
        ai_authenticate_rest_api_is_wp_rest_api_request()) {
        return new WP_Error(
            'rest_not_logged_in',
            __('You are not currently logged in.'),
            array('status' => 401)
        );
    }
 
    // Our custom authentication check should have no effect
    // on logged-in requests
    return $result;
});

