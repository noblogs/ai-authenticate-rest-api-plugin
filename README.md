Simple plugin to prevent unauthenticated access to the Wordpress REST API.

See https://developer.wordpress.org/rest-api/frequently-asked-questions/#can-i-disable-the-rest-api
for further details.

